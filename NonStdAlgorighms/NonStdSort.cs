﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace NonStdAlgorighms
{
    public class NonStdSort
    {

        public static IEnumerable<T> StalinSort<T>(IEnumerable<T> input)
        {
            return StalinSort(input, Comparer<T>.Default);
        }

        public static IEnumerable<T> StalinSort<T>(IEnumerable<T> input, IComparer<T> comparer)
        {
            if (input is null)
            {
                return null;
            }

            if (comparer is null)
            {
                throw new ArgumentException("Comparer cannot be null");
            }

            var sortedList = new LinkedList<T>();
            using (var enumerator = input.GetEnumerator())
            {
                if (!enumerator.MoveNext())
                {
                    return sortedList;
                }

                sortedList.AddLast(enumerator.Current);

                while (enumerator.MoveNext())
                {
                    var item = enumerator.Current;
                    var previous = sortedList.Last.Value;
                    var comparison = comparer.Compare(previous, item);

                    if (comparison < 1)
                    {
                        sortedList.AddLast(item);
                    }
                }
            }

            return sortedList.ToArray();
        }
    }
}