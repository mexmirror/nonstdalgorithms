using Microsoft.VisualStudio.TestTools.UnitTesting;
using NonStdAlgorighms;
using Shouldly;

namespace NonStdAlgorithmsTests
{
    [TestClass]
    public class StalinSortTests
    {
        [TestMethod]
        public void GivenAlreadySortedList_WhenSort_OrderDoesNotChange()
        {
            // Arrange
            var sortedList = new[] {1, 2, 3};

            // Act
            var result = NonStdSort.StalinSort(sortedList);

            // Assert
            result.ShouldBe(sortedList);
        }

        [TestMethod]
        public void GivenUnorderedList_WhenSort_UnorderedItemsAreEliminated()
        {
            // Arrange
            var unorderedList = new[] {1, 3, 2};

            // Act
            var result = NonStdSort.StalinSort(unorderedList);

            //Assert
            result.ShouldBe(new []{ 1, 3});
        }

        [TestMethod]
        public void GivenEmptyList_WhenSort_EmptyListIsReturned()
        {
            // Arrange
            var empty = new int[0];

            // Act
            var result = NonStdSort.StalinSort(empty);

            // Assert
            result.ShouldBeEmpty();
        }

        [TestMethod]
        public void GivenNullInput_WhenSort_NullIsReturned()
        {
            // Act
            var result = NonStdSort.StalinSort<int>(null);

            // Assert
            result.ShouldBeNull();
        }

        [TestMethod]
        public void GivenInverseIntegerComparer_WhenSort_CustomComparerIsUsed()
        {
            // Arrange
            var comparer = new InverseIntegerComparer();
            var unorderedList = new[] {3, 5, -8, 9, -100};

            // Act
            var result = NonStdSort.StalinSort(unorderedList, comparer);

            // Assert
            result.ShouldBe(new []{3, -8, -100});
        }
    }
}