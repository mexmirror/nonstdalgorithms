using System.Collections.Generic;

namespace NonStdAlgorithmsTests
{
    public class InverseIntegerComparer : IComparer<int>
    {
        private static readonly IComparer<int> DefaultIntComparer = Comparer<int>.Default;

        public int Compare(int x, int y)
        {
            return DefaultIntComparer.Compare(x, y) * -1;
        }
    }
}